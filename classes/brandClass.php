<?php 
 
 include('../lib/Database.php');
 include('../helpers/Format.php');
 ?>

<?php
	
	/**
	* Brand Class 
	*/
	class Brand {

		private $db;
		private $fm;

		public function __construct(){

			$this->db =new Database();
			$this->fm = new Format();
		}//Constructor

		public function Brand_Insert($brandName){

		$brandName = $this->fm->validation($brandName);
		$brandName = mysqli_real_escape_string($this->db->link,$brandName);

			if (empty($brandName)) {
				
				$msg ="<span class='error'>Brand name must not be empty. </span>";

				return $msg;

			}else{

				$query ="INSERT INTO tbl_brand(brandName) VALUES('$brandName') ";

				$brandinsert = $this->db->insert($query);

				if ($brandinsert) {
				
				 $msg ="<span class='success'>Brand Insert Successfully</span>";
				 return $msg;

				}else{

					$msg ="<span class='error'>Brand is not Inserted.... </span>";
					 return $msg;
				}

			}
		}//Brand_Insert

		public function getAllBrand(){
			$query ="SELECT * FROM tbl_brand ORDER BY brandId DESC";
			$result =$this->db->select($query);
			return $result;
		}//getAllBrand

		public function getBrandByid($id){
			$query= "SELECT * FROM tbl_brand WHERE brandId='$id'";

			$result =$this->db->select($query);

			return $result;
		}//Edit fetch getBrandByid

		public function brand_Update($brandName, $id){
		

			$bandNmae  =$this->fm->validation($brandName);
			$brandName =mysqli_real_escape_string($this->db->link, $bandNmae);
			$id        =mysqli_real_escape_string($this->db->link, $id);

			if (empty($bandNmae)) {
				$msg ="<span class='error'>Brand name must not be empty.</span>";

				return $msg;
			}else{
				$query ="UPDATE tbl_brand 

				SET brandName='$brandName' WHERE brandId='$id'";
				$update_band =$this->db->update($query);
				if ($update_band) {
					
					 $msg ="<span class='success'>Brand Name updated Successfully</span>";
				 return $msg;
				 }else{

				 	$msg ="<span class='error'>Brand name not updated. </span>";

				return $msg;
				 }
			}
			
		}//brand_Update method

		/*Delete Method*/

		public function delBrandById($id){
			$query ="DELETE FROM tbl_brand WHERE brandId='$id'";
			$delBrand=$this->db->delete($query);

			if ($delBrand) {
				$msg="<span class='success'>Brand Name delete Successfully</span>";
				return $msg;
			}else{
				$msg ="<span class='error'>Brand name not deleted. </span>";

				return $msg;
			}
		}/*Delete Method*/


	}//Brand Class 

?>