<?php 

	include '../lib/Session.php';
	Session::checkLogin();

	include_once '../lib/Database.php';
	include_once '../helpers/Format.php';
?>
	

<?php

	/**
	* Admin Login Class 
	*/
	class AdminLogin{
		
		private $db;
		private $fm;

		public function __construct(){
			
			$this->db = new Database();
			$this->fm = new Format();
		}
		public function adminLogin($admin_user, $admin_pass){

			$admin_user = $this->fm->validation($admin_user);
			$admin_pass = $this->fm->validation($admin_pass);

			$admin_user = mysqli_real_escape_string($this->db->link, $admin_user);
			$admin_pass = mysqli_real_escape_string($this->db->link, $admin_pass);

			if(empty($admin_user) || empty($admin_pass)){

				$loginsms = "User or Password must not be empty";
				return $loginsms;
			}else{


				//Check query

				$query ="SELECT * FROM tbl_admin WHERE admin_user='$admin_user' && admin_pass='$admin_pass'";

				$result =$this->db->select($query);

				if ($result != false) {
					
					$value = $result->fetch_assoc();

					Session::set("adminLogin", true);
					Session::set("admin_id", $value['admin_id']);
					Session::set("admin_user", $value['admin_user']);
					Session::set("admin_name", $value['admin_name']);

					header("Location:index.php");
				}else{
						$loginsms = "User or Password not match";
						return $loginsms;
				}
			}
		}
	}
?>