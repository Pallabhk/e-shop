<?php require_once('../lib/Database.php');?>
<?php require_once('../helpers/Format.php');?>
<?php

	/**
	* Product Class 
	*/
	class Product{

		private $db;
		private $fm;

		public function __construct(){
			
			$this->db =new Database();
			$this->fm = new Format();
		}//construct method

		public function productInsert($data,$file){

			$productName =$this->fm->validation($data['productName']);
			$productName = mysqli_real_escape_string($this->db->link,$data['productName']);

			$catID       =$this->fm->validation($data['catId']);
			$catID       = mysqli_real_escape_string($this->db->link,$data['catId']);

			$brandId     =$this->fm->validation($data['brandId']);
			$brandId     = mysqli_real_escape_string($this->db->link,$data['brandId']);

			$productDes =$this->fm->validation($data['productDes']);
			$productDes = mysqli_real_escape_string($this->db->link,$data['productDes']);

			$productPrice =$this->fm->validation($data['productPrice']);
			$productPrice = mysqli_real_escape_string($this->db->link,$data['productPrice']);

			$productType =$this->fm->validation($data['productType']);
			$productType = mysqli_real_escape_string($this->db->link,$data['productType']);

			/*Image File Upload*/
			
			$permited =array('jpg' ,'jpeg' ,'png' ,'gif');

			$file_name=$file['productImg']['name'];
			$file_size=$file['productImg']['size'];
			$file_temp=$file['productImg']['tmp_name'];

			$div =explode('.', $file_name);
			$file_ext=strtolower(end($div));
			$unique_image=substr(md5(time()),0,10).'.'.$file_ext;
			$uploaded_image="upload/".$unique_image;

			if ($productName == "" || $catID == "" || $brandId == "" || $productDes == "" || $productPrice == "" || $file_name=="" || $productType == "") {
				
				$msg= "<span class='error'>Fild must not be empty...!</span>";
				return $msg;
			}elseif($file_size >1048567){
				$msg= "<span class='error'>Image size shoud be less then 1 Mb!</span>";
				return $msg;
			}elseif(in_array($file_ext, $permited) ===false){
				echo "<span class='error'>You Can upload only:-".implode( ', ', $permited)."</span>";
			}else{

				move_uploaded_file($file_temp, $uploaded_image );
				$query ="INSERT INTO tbl_product(productName, catId, brandId, productDes, productPrice, productImg, productType) VALUES('$productName', '$catID', '$brandId', '$productDes', '$productPrice', '$uploaded_image', '$productType')";

				$insertPro =$this->db->insert($query);

				if ($insertPro) {
					$msg= "<span class='success'>Product Insert Successfully......</span>";
				return $msg;
				}else{

					$msg= "<span class='error'>Product Can not Inserted .....</span>";
				return $msg;
				}
			}
		}//productInsert

		public function getAllProduct(){
			//Alias query

			$query ="SELECT p.*, c.catName, b.brandName 
					FROM tbl_product as p, tbl_category as c, tbl_brand as b
					WHERE p.catId =c.catId AND p.brandId = b.brandId
					ORDER BY p.productId DESC;
			       ";

			/*Normal query
			$query ="SELECT  tbl_product.*, tbl_category.catName, tbl_brand.brandName
			FROM tbl_product
			INNER JOIN tbl_category
			ON tbl_product.catId =tbl_category.catId 
			INNER JOIN tbl_brand
			ON tbl_product.brandId =tbl_brand.brandId 
			ORDER BY tbl_product.productId DESC";

			*/

			$result =$this->db->select($query);

			return $result;
		}
		public function getProductByID($id){
			$query= "SELECT * FROM tbl_product WHERE productId='$id'";

			$result =$this->db->select($query);

			return $result;
		}//getProductByID Method

		public function productUpdate($data,$file,$id){

			$productName =$this->fm->validation($data['productName']);
			$productName = mysqli_real_escape_string($this->db->link,$data['productName']);

			$catID       =$this->fm->validation($data['catId']);
			$catID       = mysqli_real_escape_string($this->db->link,$data['catId']);

			$brandId     =$this->fm->validation($data['brandId']);
			$brandId     = mysqli_real_escape_string($this->db->link,$data['brandId']);

			$productDes =$this->fm->validation($data['productDes']);
			$productDes = mysqli_real_escape_string($this->db->link,$data['productDes']);

			$productPrice =$this->fm->validation($data['productPrice']);
			$productPrice = mysqli_real_escape_string($this->db->link,$data['productPrice']);

			$productType =$this->fm->validation($data['productType']);
			$productType = mysqli_real_escape_string($this->db->link,$data['productType']);

			/*Image File Upload*/
			
			$permited =array('jpg' ,'jpeg' ,'png' ,'gif');

			$file_name=$file['productImg']['name'];
			$file_size=$file['productImg']['size'];
			$file_temp=$file['productImg']['tmp_name'];

			$div =explode('.', $file_name);
			$file_ext=strtolower(end($div));
			$unique_image=substr(md5(time()),0,10).'.'.$file_ext;
			$uploaded_image="upload/".$unique_image;

			if ($productName == "" || $catID == "" || $brandId == "" || $productDes == "" || $productPrice == ""  || $productType == "") {
				
				$msg= "<span class='error'>Fild must not be empty...!</span>";
				return $msg;
			}else{
				 if (!empty($file_name)) {
				 
					if($file_size >1048567){
					$msg= "<span class='error'>Image size shoud be less then 1 Mb!</span>";
					return $msg;
				}elseif(in_array($file_ext, $permited) ===false){
					echo "<span class='error'>You Can upload only:-".implode( ', ', $permited)."</span>";
				}else{

					move_uploaded_file($file_temp, $uploaded_image );
					

					$query="UPDATE tbl_product
						SET
						productName ='$productName',
						catId       ='$catID' ,
						brandId     ='$brandId' ,
						productDes  ='$productDes',
						productPrice='$productPrice',
						productImg  ='$uploaded_image',
						productType ='$productType'
						
						WHERE productId ='$id'";

					$updatePro =$this->db->update($query);

					if ($updatePro) {
						$msg= "<span class='success'>Product Update Successfully......</span>";
					return $msg;
					}else{

						$msg= "<span class='error'>Product Can not Update .....</span>";
					return $msg;
					}
				}
			}else{

					$query="UPDATE tbl_product
						SET
						productName ='$productName',
						catId       ='$catID',
						brandId     ='$brandId',
						productDes  ='$productDes',
						productPrice='$productPrice',
						productType ='$productType'
						
						WHERE productId ='$id';
					";
					$updatePro =$this->db->update($query);

					if ($updatePro) {
						$msg= "<span class='success'>Product Update Successfully......</span>";
					return $msg;
					}else{

						$msg= "<span class='error'>Product Can not Update .....</span>";
					return $msg;
				}
			}
		 }
		}//productUpdate
		public function delProductById($id){
			$query ="SELECT * FROM tbl_product WHERE productId ='$id'";

			$getData =$this->db->select($query);

			if ($getData) {
				while ($delImg =$getData->fetch_assoc()) {
						$dellink= $delImg['productImg'];
						unlink($dellink);
					}	
			}
				$delquery="DELETE FROM tbl_product WHERE productId ='$id'";
				$delData =$this->db->delete($delquery);
				if ($delData) {
				$msg="<span class='success'>Product Delete Successfully</span>";
				return $msg;

				}else{
				$msg= "<span class='error'>Product Can not Deleted .....</span>";
					return $msg;
			}
		}// delProductById
	}//Product Class 
?>