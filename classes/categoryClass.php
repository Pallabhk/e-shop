<?php

require('../lib/Database.php');
require_once('../helpers/Format.php');
	
?>

<?php

	/**
	* Category Class 
	*/
	class Category{
		private $db;
		private $fm;

		public function __construct(){

			$this->db =new Database();
			$this->fm = new Format();
		}

		public function CatInsert($catName){

			$catName = $this->fm->validation($catName);
			$catName = mysqli_real_escape_string($this->db->link,$catName);

			if (empty($catName)) {
				
				$catmsg ="<span class='error'>Category name must not be empty. </span>";

				return $catmsg;

			}else{

				$query ="INSERT INTO tbl_category(catName) VALUES('$catName') ";

				$catinsert = $this->db->insert($query);

				if ($catinsert) {
				
				 $msg ="<span class='success'>Category Insert Successfully</span>";
				 return $msg;

				}else{

					$msg ="<span class='error'>Category is not Inserted.... </span>";
					 return $msg;
				}

			}
		}

		public function getAllcat(){

			$query= "SELECT * FROM tbl_category ORDER BY catId DESC";

			$result =$this->db->select($query);

			return $result;
		}
		//Update Category Method

		public function getcatByid($id){
			$query= "SELECT * FROM tbl_category WHERE catId='$id'";

			$result =$this->db->select($query);

			return $result;
		}
		 public function CatUpdate($catName, $id){

			$catName = $this->fm->validation($catName);

			$catName = mysqli_real_escape_string($this->db->link, $catName);
			$id      = mysqli_real_escape_string($this->db->link, $id);

			if (empty($catName) ) {
				
				$catmsg ="<span class='error'>Category name must not be empty. </span>";

				return $catmsg;

			}else{
				$query ="UPDATE tbl_category
					SET
					catName='$catName' WHERE catId ='$id'";

				 $Updaterow =$this->db->update($query);
				 
				 if ($Updaterow) {
				 	 $msg ="<span class='success'>Category updated Successfully</span>";
				 return $msg;
				 }else{

				 	$msg ="<span class='error'>Category name not updated. </span>";

				return $msg;
				 }
			}
		}//Update Query End 

		/*delete query start*/

		public function delCatById($id){

			$query ="DELETE  FROM tbl_category WHERE catId='$id'";

			$delData=$this->db->delete($query);
			if ($delData) {
				 $msg ="<span class='success'>Category Delete Successfully</span>";
				 return $msg;
			}else{
				 $msg ="<span class='error'>Category Not delete</span>";
				 return $msg;
			}
		}
		
	}//Main Class 
?>