﻿<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php require_once'../classes/Product_class.php';?>
<?php require_once'../helpers/Format.php';?>

<?php
	$objpro  = new Product();
	$fm      = new Format();
?>
<!--Delete method-->
<?php
	if (isset($_GET['delpro'])) {
		$id =$_GET['delpro'];

		$delproduct =$objpro->delProductById($id);
	}
?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Post List</h2>
        <div class="block"> 
        <?php
        	if (isset($delproduct)) {
        		print_r($delproduct);
        	}
        ?> 
            <table class="data display datatable" id="example">
			<thead>
				<tr>
					<th>Sl</th>
					<th>Product </th>
					<th>Category</th>
					<th>Brand</th>
					<th>Description</th>
					<th>Price</th>
					<th>Image</th>
					<th>Type</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$getPro =$objpro->getAllProduct();

					if ($getPro) {
						$i =0;
						while ($result =$getPro->fetch_assoc()) { 
							$i++; ?>

					<tr class="odd gradeX">
					<td><?php echo $i;?></td>
					<td><?php echo $result['productName'];?></td>
					<td><?php echo $result['catName'];?></td>
					<td><?php echo $result['brandName'];?></td>
					<td><?php echo $fm->textShorten($result['productDes'],50);?></td>
					<td>$<?php echo $result['productPrice'];?></td>
					<td><img src="<?php echo $result['productImg'];?>" height="40px" width="60px"></td>
					<td><?php 
						if ($result['productType'] == 0 ) {
							
							echo "Featured";
						}else{
							echo "General";
						}
					?></td>
					
					<td><a href="productedit.php?proId=<?php echo $result['productId']?>">Edit</a> || <a  onclick="return confirm('Are you sure to delete?')" href="?delpro=<?php echo $result['productId']?>">Delete</a></td>
				</tr>
				<?php	} } ?>
				
				
			</tbody>
		</table>

       </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        setupLeftMenu();
        $('.datatable').dataTable();
		setSidebarHeight();
    });
</script>
<?php include 'inc/footer.php';?>
