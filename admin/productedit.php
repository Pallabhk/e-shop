<?php
    require_once('../classes/brandClass.php');
    require_once('../classes/categoryClass.php');
    include('../classes/Product_class.php');


 ?>
<?php

    if (!isset($_GET['proId']) || $_GET['proId'] == NULL) {
        
        echo "<script>window.location = 'productist.php' </script>";
      }else{

        $id =$_GET['proId'];
      }
    //create product Object
    
    $objPro = new Product();

    if ($_SERVER['REQUEST_METHOD'] == 'POST'  && isset($_POST['submit'])) {
       
       $updateProduct =$objPro->productUpdate($_POST,$_FILES,$id);
    }
?>
<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>


<div class="grid_10">
    <div class="box round first grid">
        <h2>Add New Product</h2>
        <div class="block"> 
        <?php
            if (isset($updateProduct)) {
                echo $updateProduct;
            }
        ?> 
        <?php

            $getPro =$objPro->getProductByID($id);
            if ($getPro) {
                while ($value =$getPro->fetch_assoc()) {?>
                         
            <form action="" method="post" enctype="multipart/form-data">
            <table class="form">
               
                <tr>
                    <td>
                        <label>Product Name</label>
                    </td>
                    <td>
                        <input type="text" name="productName" value="<?php echo $value['productName'];?>" class="medium" />
                    </td>
                </tr>
				<tr>
                    <td>
                        <label>Category</label>
                    </td>
                    <td>
                        <select id="select" name="catId">
                            <!--Fetch Category-->
                             <option>Select Category</option>
                            <?php

                                $catobj = new Category();
                                $getCat =$catobj->getAllcat();

                                if ($getCat) {
                                    while ($result =$getCat->fetch_assoc()) {?>

                                   
                                   <option 

                                        <?php
                                            if ($result['catId'] == $value['catId']) {?>
                                                selected ="selected";
                                           <?php }
                                        ?>
                                   value="<?php echo $result['catId'];?>"><?php echo $result['catName'];?></option>
                           <?php }  } ?>
                           
                            
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>
                        <label>Brand</label>
                    </td>
                    <td>
                        <select id="select" name="brandId">
                            <option>Select Brand</option>
                            <?php

                                $brandobj = new Brand();
                                $getBrand =$brandobj->getAllBrand();

                                if ($getBrand) {
                                    while ($result =$getBrand->fetch_assoc()) {?>

                                   
                                   <option 
                                        <?php
                                            if ($result['brandId'] == $value['brandId']) {?>
                                                  selected ="selected";
                                           <?php } ?> value="<?php echo $result['brandId'];?>"><?php echo $result['brandName'];?>
                                               
                                    </option>
                           <?php }  } ?>
                           
                        </select>
                    </td>
                </tr>
				
				 <tr>
                    <td style="vertical-align: top; padding-top: 9px;">
                        <label>Description</label>
                    </td>
                    <td>
                        <textarea class="tinymce" name="productDes" >
                            <?php echo $value['productDes'];?>
                        </textarea>
                    </td>
                </tr>
				<tr>
                    <td>
                        <label>Price</label>
                    </td>
                    <td>
                        <input type="text" placeholder="Enter Price..."  name="productPrice" value="<?php echo $value['productPrice'];?>" class="medium" />
                    </td>
                </tr>
            
                <tr>
                    <td>
                        <label>Upload Image</label>
                    </td>
                    <td>
                        <img src="<?php echo $value['productImg'];?>"  height="80px" width="120px" alt=""><br/>
                        <input type="file" name="productImg" />
                    </td>
                </tr>
				
				<tr>
                    <td>
                        <label>Product Type</label>
                    </td>
                    <td>
                        <select id="select" name="productType">
                            <option>Select Type</option>
                                <?php if($value['productType'] == 0){?>
                            <option  selected="selected" value="0">Featured</option>
                            <option value="1">General</option>
                            <?php }else{?>
                            <option selected="selected" value="1">General</option>
                             <option value="0">Featured</option>
                            <?php }?> 
                        </select>
                    </td>
                </tr>

				<tr>
                    <td></td>
                    <td>
                        <input type="submit" name="submit" Value="Update"/>
                    </td>
                </tr>
            </table>
            </form>
            <?php } } ?>
        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<!-- Load TinyMCE -->
<?php include 'inc/footer.php';?>


